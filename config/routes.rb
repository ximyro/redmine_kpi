# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
resources :projects do
 member do
  get 'kpi', :to => 'kpi#index'
 end
end

