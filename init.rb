require 'redmine'
require 'gchart'


Redmine::Plugin.register :kpi do
  name 'Kpi plugin'
  author 'Author name'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'


    project_module :kpi do
   # permission :view_kpi, :kpi => :index
    permission :kpi, :kpi => :index
  end

#  permission :kpi, { :kpi => [:index] }, :public => true
  menu :project_menu, :kpi, { :controller => 'kpi', :action => 'index' }, :caption => 'KPI'#, :param => :project_id

end
